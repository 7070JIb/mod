﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPIFramework
{
    public class ChatMessageEventArgs
    {
        public string Username { get; set; }
        public string Message { get; set; }
    }
}
