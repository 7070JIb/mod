﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPIFramework
{
    public interface IChatEventProcessor
    {
        void OnMessageGet(object sender, ChatMessageEventArgs args);
        void OnViewerJoined(object sender, string nickname);
        void OnViewerLeft(object sender, string nickname);
    }
}
