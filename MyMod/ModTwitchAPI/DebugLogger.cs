﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RimWorld;
using Verse;

namespace MyMod.TwitchAPI
{
    public class DebugLogger : TwitchAPIFramework.ILogger
    {
        public void Log(List<Tuple<string, ConsoleColor>> messagesColoured)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in messagesColoured)
            {
                sb.Append(item.Item1);
            }
            Verse.Log.Message(sb.ToString());
        }

        public void Log(string message, ConsoleColor color = ConsoleColor.Black)
        {
            Verse.Log.Message(message.ToString());
        }

        public void Log(string message)
        {
            Verse.Log.Message(message);
        }
    }
}
