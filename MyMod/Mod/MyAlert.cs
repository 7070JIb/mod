﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using UnityEngine;
using Verse;
using MyMod.TwitchAPI;

namespace MyMod
{
    public class MyAlert : Alert
    {
        public MyAlert()
        {
            this.defaultLabel = "Viewers count: ...";
            this.defaultPriority = AlertPriority.Critical;
        }
        public override TaggedString GetExplanation()
        {
            StringBuilder sb = new StringBuilder("Список зрителей: \n");
            foreach (string viewerName in MyMod.twitch.Bot.Viewers)
            {
                sb.AppendLine(viewerName);
            }
            return sb.ToString();
        }
        public override string GetLabel()
        {
            return "Viewers count: ..." + MyMod.twitch.Bot.Viewers.Count;
        }
        public override AlertReport GetReport()
        {
            return (MyMod.twitch.Bot.IsBotAuthorized) ? true : false;
        }
    }
}
