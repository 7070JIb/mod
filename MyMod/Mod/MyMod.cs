﻿//using Newtonsoft.Json;
using RimWorld;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using Verse;
using UnityEngine;
using MyMod.TwitchAPI;
using TwitchAPIFramework;
using System.Collections.Generic;

namespace MyMod
{
    //public class ExampleMod : Mod
    //{
    //    /// <summary>
    //    /// A reference to our settings.
    //    /// </summary>
    //    TwitchSettings settings;

    //    /// <summary>
    //    /// A mandatory constructor which resolves the reference to our settings.
    //    /// </summary>
    //    /// <param name="content"></param>
    //    public ExampleMod(ModContentPack content) : base(content)
    //    {
    //        this.settings = GetSettings<TwitchSettings>();
    //    }

    //    /// <summary>
    //    /// The (optional) GUI part to set your settings.
    //    /// </summary>
    //    /// <param name="inRect">A Unity Rect with the size of the settings window.</param>

    //    public override void DoSettingsWindowContents(Rect inRect)
    //    {
    //        Listing_Standard listingStandard = new Listing_Standard();
    //        listingStandard.Begin(inRect);
    //        listingStandard.TextEntryLabeled("example label", settings.botName);
    //        listingStandard.End();
    //        base.DoSettingsWindowContents(inRect);
    //    }

    //    /// <summary>
    //    /// Override SettingsCategory to show up in the list of settings.
    //    /// Using .Translate() is optional, but does allow for localisation.
    //    /// </summary>
    //    /// <returns>The (translated) mod name.</returns>
    //    public override string SettingsCategory()
    //    {
    //        return "MyExampleModName".Translate();
    //    }
    //}

    
    [StaticConstructorOnStartup]
    public static class MyMod
    {
        public static TwitchConnectionManager twitch;
        public static TwitchAPIFramework.ILogger myLogger;
        static MyMod()
        {
            Log.Message("RIMWORLD2");
            myLogger = new TwitchAPI.DebugLogger();
            twitch = new TwitchConnectionManager(myLogger, false);
            twitch.CreateBot(new ChatEventProcessor());
            twitch.JoinChannel("maxmontana");
        }
    }
}



