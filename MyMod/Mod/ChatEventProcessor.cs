﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchAPIFramework;
using RimWorld;
using Verse;

namespace MyMod
{
    public class ChatEventProcessor : IChatEventProcessor
    {
        public static Stack<ChatMessageEventArgs> messageEventsToFire = new Stack<ChatMessageEventArgs>();
        public void OnMessageGet(object sender, ChatMessageEventArgs args)
        {
            messageEventsToFire.Push(args);
            Messages.Message(args.Username + ": " + args.Message, MessageTypeDefOf.NeutralEvent);
        }

        public static Stack<string> viewerJoinedEventsToFire = new Stack<string>();
        public void OnViewerJoined(object sender, string nickname)
        {
            viewerJoinedEventsToFire.Push(nickname);
            Messages.Message("+++" + nickname, MessageTypeDefOf.NeutralEvent);
        }

        public static Stack<string> viewerLeftEventsToFire = new Stack<string>();
        public void OnViewerLeft(object sender, string nickname)
        {
            viewerJoinedEventsToFire.Push(nickname);
            Messages.Message("---" + nickname, MessageTypeDefOf.NeutralEvent);
        }
    }
}
