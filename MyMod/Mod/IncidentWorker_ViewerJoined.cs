﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;

namespace MyMod
{
    public class IncidentWorker_ViewerJoined : IncidentWorker
    {

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            MyMod.myLogger.Log("VIEWER JOINED CAN FIRE NOW " + (base.CanFireNowSub(parms) && MyMod.twitch.Bot.IsBotAuthorized && ChatEventProcessor.viewerJoinedEventsToFire.Count > 0));

            return base.CanFireNowSub(parms) && MyMod.twitch.Bot.IsBotAuthorized && ChatEventProcessor.viewerJoinedEventsToFire.Count>0;
        }
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            MyMod.myLogger.Log("VIEWER JOINED EXECUTION");
            string nickname= ChatEventProcessor.viewerJoinedEventsToFire.Pop();

            this.SendStandardLetter("Meet " + nickname, nickname + " joined channel",LetterDefOf.NeutralEvent, parms,LookTargets.Invalid, Array.Empty<NamedArgument>()); 

            return true;
        }
    }
}
